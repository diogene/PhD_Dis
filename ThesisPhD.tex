%% Copyright (C) 2017  Diogene Alessandro Dei Tos
%%
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%      https://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

% !TEX spellcheck = en-en_US
%
\documentclass[b5paper,10pt,twoside,openright,english,italian]{book}
%
%%%%%%%% Packages %%%%%%%%
%\usepackage[cam,center,a4,pdflatex,axes]{crop}
\usepackage[twoside=true]{geometry}
\usepackage{phdthesis,phdtitle}
\usepackage{fancyhdr,color,array,amsmath,amssymb,graphicx,subfig}
\usepackage{mdwmath,mdwtab}
\usepackage{glossaries,listings,booktabs,latexsym,url,bnf,rotating,multirow,multicol,pdflscape}
\usepackage{paralist,lscape,longtable,epigraph}
\usepackage[bookmarks=true,pdftex=false,bookmarksopen=true,pdfborder={0,0,0}]{hyperref}
\usepackage{algorithm,algpseudocode,algorithmicx}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage{setspace,parskip,bm}
\usepackage{amsthm,mathrsfs,tabularx}
\usepackage[justification=centering]{caption}
\usepackage{hyphenat,tikz,tikz-3dplot,xspace,mathtools}
\usepackage{marginnote,gensymb,enumerate,wasysym,wrapfig}
\usepackage[bottom]{footmisc}
\usepackage[english,italian]{babel}
\usepackage{CJK,arabtex}
\usepackage[page]{appendix}
\usepackage{ulem,lipsum,morewrites}
\usepackage[skins,theorems,most]{tcolorbox}
\usepackage[sorting=none,
style=numeric-comp,%
%sortcites=true,%
bibencoding=ascii,%
autopunct=true,%
hyperref=true,%
language=auto,%
backref=true,%
doi=true,%
url=false,%
maxcitenames=3,%
maxbibnames=50,%
natbib=true,%
%giveninits,%
isbn=false,%
backend=bibtex]{biblatex}
\addbibresource{Biblio.bib}
\defbibheading{mybibheadings}{\chapter*{\bibname}\markboth{\bibname}{}}
\usetikzlibrary{shapes.geometric}
\normalem
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

% Set paper size
%\geometry{paperwidth=17cm, paperheight=24cm, margin=2cm, top=2.3cm, bindingoffset=0.4cm}
%\geometry{a4paper, margin=3cm, top=3.8cm, bindingoffset=0.4cm}
\geometry{b5paper,margin=2cm,top=2.3cm,bindingoffset=0.4cm}

%%%%%%%% Graphics %%%%%%%%
\graphicspath{{gfx/logos/}{gfx/chap1/}}

%%%%%%%% Hyphenaton exception %%%%%%%%
\hyphenation{nonauto-nomous}
\hyphenation{four-dimen-sional}

%%%%%%%% Theorems and definitions %%%%%%%%
\theoremstyle{remark}
%\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{Definition}{Definition}[section]
\newtheorem{pb_statemenent}{Problem}
\newtheorem{theorem}{Theorem}[]

% Common options
\lstset{tabsize=2,basicstyle=\footnotesize,breaklines=true}
\hypersetup{pdftitle={Title}, pdfauthor={Author}}

% Front matter
\department{\myDept}
\phdprogram{Doctoral Programme in Aerospace Engineering}
\author{\myName}
\title{My Title}
\advisor{\mySupervisor}
\tutor{\myTutor}
\supervisor{\myCoordinator}
\titleimage{logopoli}
\phdcycle{Academic year xxxx/xx -- Cycle XXXIII}

%%%%%%%% Acronyms and Glossary %%%%%%%%
%\setlength{\bibitemsep}{0.5\baselineskip}
%\makenoidxglossaries
\makeglossaries
\input{ThesisAcronyms}

%%%%%%%% New commands %%%%%%%%
\newcommand{\de}{\partial}
\newcommand{\di}{{\rm d}}
\newcommand{\ds}[1]{\displaystyle{#1}}
\newcommand{\vect}[1]{\boldsymbol{#1}}
\newcommand{\Eq}[1]{Eq.~\eqref{#1}}
\newcommand{\Eqs}[1]{Eqs.~\eqref{#1}}
\newcommand{\Eqss}[2]{Eqs.~\eqref{#1}--\eqref{#2}}
\newcommand{\Fig}[1]{Figure~\ref{#1}}
\newcommand{\Figs}[2]{Figures~\ref{#1}--\ref{#2}}
\newcommand{\Table}[1]{Table~\ref{#1}}
\newcommand{\iy}{$\infty$}
\newcommand{\ihline}{\hline\noalign{\smallskip}}
\newcommand{\mhline}{\noalign{\smallskip}\hline\noalign{\smallskip}}
\newcommand{\fhline}{\noalign{\smallskip}\hline}
\newcommand{\myUni}{Politecnico di Milano}
\newcommand{\myDept}{Dept.\ of Aerospace Science and Technology}
\newcommand{\myName}{My name}
\newcommand{\mySupervisor}{My Supervisor}
\newcommand{\myTutor}{My Tutor}
\newcommand{\myCoordinator}{PhD Coordinator}
\newcommand{\ie}{i.\,e.,~}
\newcommand{\Ie}{I.\,e.,~}
\newcommand{\eg}{e.\,g.,~}
\newcommand{\Eg}{E.\,g.,~}
\newcommand{\tr}{\textcolor{orange}}
\newcommand{\tb}{\textcolor{blue}}
\newcommand{\tg}{\textcolor{green}}
\colorlet{BLUE}{blue}
\colorlet{ORANGE}{orange}
\definecolor{Lightgray}{rgb}{0.9, 0.9, 0.9}
\newcommand{\Mnota}[2]{\marginpar{\footnotesize{\tr{#1} \\ \tb{#2}}}}
\newcommand{\ruru}{1.6mm}
\newcommand{\TOPlines}{\hline\hline\noalign{\vskip\ruru}}
\newcommand{\MIDline}{\noalign{\vskip\ruru}\hline\noalign{\vskip\ruru}}
\newcommand{\BOTTOMlines}{\noalign{\vskip\ruru}\hline\hline}
\newtcolorbox{mybox}{enhanced jigsaw,colback={Lightgray},colframe=black,boxrule=0.5pt}



% To mark draft thesis
%\usepackage{draftwatermark}
%\SetWatermarkText{DRAFT }
%\SetWatermarkScale{3.3}
\providecommand{\mLyX}{L\kern-.1667em\lower.25em\hbox{Y}\kern-.125emX\@}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Let's Start The Real Document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\selectlanguage{english}
\maketitle
\cleardoublepage
\pagestyle{fancy}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dedication
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\include{FrontBackmatter/Dedication}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\include{FrontBackmatter/Copyright}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\addcontentsline{toc}{chapter}{Abstract}\input{FrontBackmatter/abstract}
%\thispagestyle{RuRu}
\cleardoublepage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Acknowledgement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\addcontentsline{toc}{chapter}{Acknowledgements}\input{FrontBackmatter/acknowledgement}
%\thispagestyle{RuRu}
\cleardoublepage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\contentsname}{Table of Contents}
\tableofcontents
\cleardoublepage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lists of figures and tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\addcontentsline{toc}{chapter}{List of Figures}\listoffigures
%\begingroup
%\let\clearpage\relax
\addcontentsline{toc}{chapter}{List of Tables}\listoftables\cleardoublepage
%\endgroup


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lists of acronyms and symbols
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\printglossary[type=\acronymtype,title=List of Acronyms]\cleardoublepage
%\addcontentsline{toc}{chapter}{List of Acronyms}
%\makeglossaries[title=List of Acronyms]
%{\let\clearpage\relax\printnoidxglossary[title=List of Symbols]}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main matter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Arabic numbering
\setcounter{page}{1}
\pagenumbering{arabic}
%
%%%%%% Part I
\part{Natural motion and ballistic capture dynamics}\label{part:Natural motion and ballistic capture dynamics}
%
% Chapter 1
\input{chapters/chapter1_Intro}\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Appendices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{appendices}
	% Appendix A
	\input{chapters/AppendixA}\cleardoublepage
\end{appendices}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\pagestyle{fancy}
\phantomsection
\addcontentsline{toc}{chapter}{\bibname}
%\bibliographystyle{aiaa}\bibliography{Biblio}
\printbibliography[heading=mybibheadings]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colophon
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\cleardoublepage\include{FrontBackmatter/Colophon}


\end{document}

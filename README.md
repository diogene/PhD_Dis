# Template for PhD thesis writing

This repository contains a template for the typographical look-and-feel "PhD_Dis" developed by Diogene Alessandro Dei Tos to write a PhD thesis. The template is written for both Latex and Lyx.


## How to use this template

1. Check it out from GitLab.
2. Create a new local repository and copy the files from this repo into it.
3. Start writing your thesis!

``` shell
git clone https://gitlab.com/diogene/PhD_Dis.git
cd PhD_Dis
touch test.dlm
git add test.dlm
git commit -m 'Boilerplate for mods in PhD_Dis'
```

## Source Code Headers

Every file containing source code must include copyright and license
information. This includes any JS/CSS files that you might be serving out to
browsers. (This is to help well-intentioned people avoid accidental copying that
doesn't comply with the license.)

Apache header:

    Copyright 2017-2020 Diogene Alessandro Dei Tos.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        https://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
